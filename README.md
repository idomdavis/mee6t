# mee6t

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/mee6t?style=plastic)](https://bitbucket.org/idomdavis/mee6t/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/mee6t?style=plastic)](https://bitbucket.org/idomdavis/mee6t/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/mee6t?style=plastic)](https://bitbucket.org/idomdavis/mee6t/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/mee6t)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

Simple AWS Lambda function to get Mee6 XP values for a group
