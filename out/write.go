package out

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"

	"bitbucket.org/idomdavis/mee6t/group"
)

// Write the given users as CSV to the writer. The columns are:
//    ID, Name, Level, Messages, XP
func Write(w io.Writer, users []group.User) error {
	o := csv.NewWriter(w)

	for _, u := range users {
		_ = o.Write([]string{u.ID, u.Name, strconv.Itoa(u.Level),
			strconv.Itoa(u.Messages), strconv.Itoa(u.XP)})
	}

	o.Flush()

	if o.Error() != nil {
		return fmt.Errorf("failed to write users: %w", o.Error())
	}

	return nil
}
