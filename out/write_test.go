package out_test

import (
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/mee6t/group"
	"bitbucket.org/idomdavis/mee6t/out"
)

func ExampleWrite() {
	u := []group.User{
		{
			ID:       "1",
			Name:     "User",
			Messages: 2,
			Level:    1,
			XP:       25,
		},
	}

	w := strings.Builder{}
	err := out.Write(&w, u)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(w.String())

	// Output:
	// 1,User,1,2,25
}

func TestWrite(t *testing.T) {
	t.Run("An invalid writer will error", func(t *testing.T) {
		err := out.Write(mockWriter{}, []group.User{{}})

		switch {
		case err == nil:
			t.Error("expected error, got nil")
		case !strings.HasPrefix(err.Error(), "failed to write"):
			t.Errorf("unexpected error: %v", err)
		}
	})
}

type mockWriter struct{}

func (mockWriter) Write(b []byte) (int, error) {
	return len(b), fmt.Errorf("%s", b)
}
