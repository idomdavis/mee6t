package request

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/idomdavis/mee6t/group"
	"bitbucket.org/idomdavis/mee6t/out"
	"bitbucket.org/idomdavis/mee6t/s3"
)

const (
	// Mee6URL is the base URL for the Mee6 stats
	Mee6URL = "https://mee6.xyz/api/plugins/levels/leaderboard"

	// Suffix is appended to files written to s3
	Suffix = ".csv"
)

// Handle a lambda event.
func Handle(event Event) error {
	b := strings.Builder{}
	name := time.Now().Format("02-01-2006T150405") + Suffix

	if u, err := group.Users(Mee6URL, event.Group); err != nil {
		return fmt.Errorf("failed to get users: %w", err)
	} else if err := out.Write(&b, u); err != nil {
		return fmt.Errorf("failed to write user data: %w", err)
	} else if err := s3.Upload(event.Bucket, name, b.String()); err != nil {
		return fmt.Errorf("failed to upload users: %w", err)
	}

	return nil
}
