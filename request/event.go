package request

// Event provided by the lambda.
type Event struct {
	// Group to check mee6 for.
	Group string

	// Bucket to store results in.
	Bucket string
}
