package group

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type results struct {
	Players []User
}

// Users in a Mee6 group. This will return a set of User types, or an error if
// there was a problem retrieving the users.
func Users(url, group string) ([]User, error) {
	var res results

	r, err := http.Get(fmt.Sprintf("%s/%s", url, group))

	if err != nil {
		return []User{}, fmt.Errorf("failed to get users: %w", err)
	}

	defer func() { _ = r.Body.Close() }()

	b, _ := ioutil.ReadAll(r.Body)

	err = json.Unmarshal(b, &res)

	if err != nil {
		return []User{}, fmt.Errorf("failed to read users: %w", err)
	}

	return res.Players, nil
}
