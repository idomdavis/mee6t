package group

// User in a group.
type User struct {
	// ID of the user. This will be unique and should not change.
	ID string `json:"id"`

	// Name of the user, this may change.
	Name string `json:"username"`

	// Messages the user has sent as a count.
	Messages int `json:"message_count"`

	// Level of the user.
	Level int `json:"level"`

	// XP of the user.
	XP int `json:"xp"`
}
