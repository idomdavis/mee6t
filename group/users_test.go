package group_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/mee6t/group"
)

func TestUsers(t *testing.T) {
	t.Run("A valid response will produce valid users", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "application/json")
				w.Header().Set("Location", "http://localhost")
				w.WriteHeader(http.StatusOK)
				_, _ = fmt.Fprintln(w,
					`{
						"players": [{
							"id": "12345", 
							"username": "Dom",  
							"message_count": 2568, 
							"level": 27, 
							"xp": 51386
						}]
					}`)
			}))

		u, err := group.Users(server.URL, "1")

		switch {
		case err != nil:
			t.Errorf("unexpected error: %v", err)
		case len(u) != 1:
			t.Errorf("expected 1 user, got %d", len(u))
		case u[0].Name != "Dom":
			t.Errorf("unexpected username: %s", u[0].Name)
		}
	})

	t.Run("An invalid URL will error", func(t *testing.T) {
		u, err := group.Users("", "")

		switch {
		case err == nil:
			t.Error("expected error, got nil")
		case !strings.HasPrefix(err.Error(), "failed to get users"):
			t.Errorf("unexpected error: %v", err)
		case len(u) != 0:
			t.Errorf("expected 0 users, got %d", len(u))
		}
	})

	t.Run("Invalid results will error", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "application/json")
				w.Header().Set("Location", "http://localhost")
				w.WriteHeader(http.StatusOK)
				_, _ = fmt.Fprintln(w, "")
			}))

		u, err := group.Users(server.URL, "1")

		switch {
		case err == nil:
			t.Error("expected error, got nil")
		case !strings.HasPrefix(err.Error(), "failed to read users"):
			t.Errorf("unexpected error: %v", err)
		case len(u) != 0:
			t.Errorf("expected 0 users, got %d", len(u))
		}
	})
}
