package main

import (
	"github.com/aws/aws-lambda-go/lambda"

	"bitbucket.org/idomdavis/mee6t/request"
)

func main() {
	lambda.Start(request.Handle)
}
