module bitbucket.org/idomdavis/mee6t

go 1.14

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.32.10
)
